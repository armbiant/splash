# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = source
BUILDDIR      = build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

# Make the documentation
doc:
	@$(SPHINXBUILD) -b html -D language=en source public/en
	@$(SPHINXBUILD) -b html -D language=fr source public/fr
	cp source/index.html public/

intl:
	make gettext
	sphinx-intl update -p build/gettext -l fr

htmlgitlabdocs:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./documentation/splash/' source public/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./documentation/splash/' source public/fr
	cp source/index.html public/
	
htmllocaltest:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./public/' source public/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./public/' source public/fr
	cp source/index.html public/

htmlgitlab:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./splash/' source public/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./splash/' source public/fr
	cp source/index.html public/

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
