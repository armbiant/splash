===
FAQ
===

This section lists Frequently Asked Questions (FAQ) regarding Splash and its calibration.

If you cannot find your question in this list, consider opening an issue on `Gitlab in the issue tracker <https://gitlab.com/sat-metalab/splash/issues>`__ .


Installation
============

Compilation fails although I have followed closely the build instructions
-------------------------------------------------------------------------

The most obvious reason is that you did not install the OpenGL drivers for your graphic card. You can do it with the following commands:

-  Nvidia graphic card:

.. code:: bash

     sudo apt install nvidia-cuda-toolkit

-  AMD or Intel card:

.. code:: bash

     sudo apt-add-repository ppa:oibaf/graphics-drivers
     sudo apt update
     sudo apt install mesa

Blender addon returns an error on activation
--------------------------------------------

If the error is related to the ``multiarray`` class of ``numpy``, the issue is due to a discrepancy between the installed ``numpy`` module and the version of the Python interpreter bundled in Blender. A way to fix it is to install ``numpy`` for the correct Python version in the user space.

For example if Blender has been linked against Python 3.7 (which is the case when writing these lines), on Ubuntu:

.. code:: bash

  sudo apt install python3.7
  python3.7 -m pip --user numpy

You should now be able to activate the addon correctly.


Crash
=====

Splash closes at startup with the following message
---------------------------------------------------

.. code-block:: console

  splash: pcm.c:2757: snd_pcm_area_copy: Assertion `src < dst || src >= dst + bytes' failed.
  Aborted (core dumped)

*Solution*: This issue is related to a bug in Alsa / PulseAudio. It can be solved simply by updated your system.


Usage
=====

I launch Splash and I cannot see any GUI
----------------------------------------

The GUI is hidden by default, the shortcut to show it is **ctrl+Tab**. Note that if no specific GUI window has been set, the GUI will be accessible from the first window created.
   
I cannot see any image on my 3D model
-------------------------------------

For the image to be projected correctly, the 3D model needs to have some texture coordinates set (also known as UV coordinates). The texture coordinates can be added from the 3D modeling software you used to create the model (i.e. Blender).

I can load images, but I get errors when I load videos
------------------------------------------------------

Be sure to change the media type to ``video`` before trying to load a video. This can be done in the Media panel.

Splash keeps complaining about ‘No protocol specified’, and GLFW is unable to initialize’ 
-----------------------------------------------------------------------------------------
   
If you are on Ubuntu 17.10, Wayland is running alongside Xorg when using a window manager which does not support Wayland. You need to force the display server ID, using the -S option of Splash (see ``splash --help``)


Performances
============

Splash is not very CPU hungry by today's standards, but it can be demanding on the GPU side as the number of connected displays and the output resolutions go up. Also other non-compute related issues can arise, leading to lower perceived quality of the overall installation.

The displayed content is choppy, even though the GPU framerates match the projectors'
-------------------------------------------------------------------------------------

By default the ``World framerate`` is set to 10, and it adapts to the framerates of the video inputs. For example if the input video (live or film) is set to 60Hz, the ``World framerate`` should approximately match. When it's not the case, there can be multiple source for the issue depending on the media type. Here follows a few hints for some known cases.

- Live input from a Video4Linux2 capture card:

  - if the capture card is a PCI-Express device, check that it has access to its full bandwidth. For example, a PCI-Express 4x card should be placed in a (at least) PCI-Express 4x port on the motherboard
  - check that the computer has enough memory bandwidth, and in particular check that the RAM banks are installed to use multiple data channel. High input resolution can need a lot of bandwidth.

- Live input from a NDI source:

  - check that the computer has enough memory bandwidth, and in particular check that the RAM banks are installed to use multiple data channel. High input resolution can need a lot of bandwidth.

- Film read from the filesystem:

  - check that the storage device has enough bandwidth to be able to read the video file in real time.
  - for high resolution content, use dedicated compression format. See the :doc:`../explanations/best_practices` for more details in this regard



3D models
=========

The texture coordinates do not match the planar surface I’m projecting onto, despite the corners being placed correctly
-----------------------------------------------------------------------------------------------------------------------

This happens when the original 3D model for this surface is a quad. Due to its internal working, Splash converts quads to triangles automatically. This process can lead in some cases to deformations of the texture coordinates. For now the easiest way to fix this is to subdivide the 3D model prior to exporting it for Splash. Note that this behavior can also be seen in the 3D modeler.

.. figure:: ../_static/images/quads_subdiv.jpg
   :alt: Quads subdivision to fix UV errors

   Quads subdivision to fix UV errors
