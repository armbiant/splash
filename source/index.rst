Splash
======

.. container:: index-locales

   `En <../en/index.html>`__ -=- `Fr <../fr/index.html>`__

.. container:: index-intro

   welcome to the Splash documentation website
  
.. container:: index-menu

   `-` :doc:`/contents` - `gitlab <https://gitlab.com/sat-mtl/tools/splash>`__ - `about us <https://sat.qc.ca/fr/recherche/metalab>`__ - `get in touch <https://gitlab.com/sat-mtl/tools/splash/splash/-/issues>`__ -

.. container:: index-section

   what is Splash?
  
Splash is a free video mapping software, published under the GPLv3 licence. Splash takes care of automatically calibrating the videoprojectors (intrinsic and extrinsic parameters, blending and color), and feeding them with the input video sources. This process is based on 3D models of the projection surfaces, usually created with another tool. There are some works in progress to automate the creation of the 3D models using our library `Calimiro <https://gitlab.com/sat-mtl/tools/splash/calimiro>`__.

Splash can handle an unlimited number of outputs, mapped to multiple 3D models. It has been tested with up to eight outputs on two graphic cards on a single computer. Support for multiple synchronized computers is planned, and Splash is known to run on ARM hardware too. In particular it runs well on Nvidia Jetson embedded computers.

.. container:: index-section

   Splash demo
  
The following video demonstrates the different steps to using Splash, from building configuration in Blender through a dedicated plugin to the projection test with real content.

.. raw:: html

   <div style="padding:56.25% 0 0 0;position:relative;"><iframe title="Splash demo video" src="https://player.vimeo.com/video/314519230?color=ff9933&api=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

.. container:: index-section

   why Splash?
  
Splash was started at a time where no other free video mapping software capable of handling any kind of projection surface to create an immersive space was available. Other software were either limited to domes or could not handle more than one output, or are not free software. With Splash we aim at facilitating the deployment of immersive experiences of any scale, from livingroom experiences to planetariums.
  
.. container:: index-section

   table of contents
   
* how to :doc:`install <install/contents>`
* :doc:`a first tutorial <tutorials/first_steps>` to get you started
* :doc:`tutorials <tutorials/contents>` to grow your skills
* :doc:`how-to guides <howto/contents>` for step-by-step instructions to accomplish specific tasks
* :doc:`Frequently Asked Questions (FAQ) <faq/contents>` for your troubleshooting needs
  
.. container:: index-section

   get in touch

This free, libre open source software was made by the Metalab team.

To know more about us, visit `Metalab <https://sat.qc.ca/fr/recherche/metalab>`__.

Get in touch with us by `asking questions on the issue tracker <https://gitlab.com/groups/sat-mtl/tools/splash/-/issues>`__ or through channel #splash on `Libera.chat IRC chatroom <https://web.libera.chat>`__.
  
.. container:: index-section

   contribute to the project

If you wish to contribute to the project, please read the `Code of Conduct <https://gitlab.com/sat-mtl/documentation/splash/-/blob/main/CODE_OF_CONDUCT.md>`__ and the `Contributing <https://gitlab.com/sat-mtl/documentation/splash/-/blob/main/CONTRIBUTING.md>`__ guide.
  
.. container:: index-section

   please show me the code!
   
For more information visit `the code repository <https://gitlab.com/sat-mtl/tools/splash>`__.
  
.. container:: index-section

   sponsors

This project is made possible thanks to the `Society for Arts and Technology <http://www.sat.qc.ca>`__ (also known as SAT).
