Compile manually
================

You can also compile Splash by hand, especially if you are curious about
its internals or want to tinker with the code (or even, who knows,
contribute!). Note that although what follows compiles the develop
branch, it is more likely to contain bugs alongside new features /
optimizations so if you experience crash you can try with the master
branch.

The packages necessary to compile Splash are the following: 

- Ubuntu 20.04 and derivatives:

.. code:: bash

   sudo apt install build-essential git-core cmake libxrandr-dev libxi-dev \
       mesa-common-dev libgsl0-dev libatlas3-base libgphoto2-dev libz-dev \
       libxinerama-dev libxcursor-dev python3-dev yasm portaudio19-dev \
       python3-numpy libopencv-dev libjsoncpp-dev libx264-dev \
       libx265-dev zip

   # Non mandatory libraries needed to link against system libraries only
   sudo apt install libglfw3-dev libglm-dev libavcodec-dev libavformat-dev \
       libavutil-dev libswscale-dev libsnappy-dev libzmq3-dev libzmqpp-dev

-  Ubuntu 18.04 and derivatives: Splash needs a recent version of CMake
   (3.12 or newer), as well as GCC 8 or newer. For CMake this implies
   using `Kitware’s PPA <https://apt.kitware.com/>`__:

.. code:: bash

   sudo apt-get install apt-transport-https ca-certificates gnupg software-properties-common wget
   wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null
   sudo apt-add-repository 'deb https://apt.kitware.com/ubuntu/ focal main'
   sudo apt-get update
   sudo apt install cmake gcc-8 g++-8

Then install the dependencies listed for Ubuntu 20.04. Later, when
building Splash, you will need to specify which compiler to use to
CMake:

.. code:: bash

   (...)
   CC=gcc-8 CXX=g++-8 cmake ..
   (...)

-  Archlinux (not well maintained, please signal any issue):

.. code:: bash

   pacman -Sy git cmake make gcc yasm pkgconfig libxi libxinerama libxrandr libxcursor libjsoncpp
   pacman -Sy mesa glm gsl libgphoto2 python3 portaudio zip zlib

Once everything is installed, you can go on with building Splash. To
build and link it against the bundled libraries:

.. code:: bash

   git clone https://gitlab.com/sat-metalab/splash
   cd splash
   git submodule update --init
   ./make_deps.sh
   mkdir -p build && cd build
   cmake ..
   make -j$(nproc) && sudo make install

Otherwise, to build Splash and link it against the system libraries:

.. code:: bash

   git clone https://gitlab.com/sat-metalab/splash
   cd splash
   git submodule update --init
   mkdir -p build && cd build
   cmake -DUSE_SYSTEM_LIBS=ON ..
   make -j$(nproc) && sudo make install

You can now try launching Splash:

.. code:: bash

   splash --help

If you want to have access to realtime scheduling within Splash, you
need to create a group “realtime”, add yourself to it and set some
limits:

.. code:: bash

   sudo addgroup realtime
   sudo adduser $USER realtime
   sudo cp ./data/config/realtime.conf /etc/security/limits.d/

And if you want the logs to be written to /var/log/splash.log:

.. code:: bash

   sudo adduser $USER syslog

Then log out and log back in.

If you want to specify some defaults values for the objects, you can set
the environment variable SPLASH_DEFAULTS with the path to a file
defining default values for given types. An example of such a file can
be found in
`data/config/splashrc <https://gitlab.com/sat-metalab/splash/tree/master/data/config/splashrc>`__

To uninstall Splash when installed from the sources, do the following from the directory Splash was built in:

.. code:: bash

    cd ${PATH_TO_SPLASH}/build
    sudo make uninstall
