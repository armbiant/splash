Install with a Ubuntu package
=============================

Download the package
--------------------

Here are the links to the Debian packages for:

- `Ubuntu 20.04 <https://gitlab.com/sat-mtl/tools/splash/splash/-/jobs/artifacts/master/download?job=package:debian:20.04>`__.
- `Ubuntu 22.04 <https://gitlab.com/sat-mtl/tools/splash/splash/-/jobs/artifacts/master/download?job=package:debian:22.04>`__.

Install the package
-------------------

The current release of Splash is also packaged for Ubuntu (version
20.04) and derived. This is done through a Debian archive, and the link can 
be found in the previous section. To install the package: 

.. code:: bash

   sudo apt install <download path>/splash.deb

Uninstall Splash
----------------

The process to uninstall Splash is the same as any other software installed on Ubuntu.

It can be done from the Software manager, or from the command line:

.. code:: bash

   sudo apt remove splash-mapper
