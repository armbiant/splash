Installation
============

For the :ref:`First steps with Splash` tutorial, you only need to install Splash.

The other featured examples in the :ref:`Tutorials` section relies on the Blender exporter. You need to install
`Blender <https://blender.org>`__ as well as the :ref:`Blender addon` on top of installing Splash. After successfully completing the installation, you will be ready for the :ref:`Single Projector Example` tutorial.  

Download and install Splash
---------------------------

To get started with Splash on your workstation, you will first need to download 
Splash before installing it.

Here you will find instructions on how to do these two steps depending on your 
set-up and usecase.

.. toctree::
   :maxdepth: 1
   :caption: Available set-ups:
   
   ubuntu_package
   Flatpak installation (recommended) <flatpak>
   metalab_ppa
   manual_compilation
   

--------------------------------------------------

Blender addon
-------------

Once Splash is compiled, a Blender addon is packaged, ready for
installation. This addon allows to:

-  create a draft scene (textured objects + cameras) and export them as
   a Splash configuration file,
-  send in real-time, through shmdata, meshes and textures from Blender
   to Splash.

To install it, download it from here:
`download Blender
addon <https://sat-metalab.gitlab.io/splash/blender_splash_addon.tar.bz2>`__. 
Open the Blender User
Preferences window, select the “Add-ons” panel, then “Install from
File…”. Navigate to the download directory and select
``blender_splash_addon.tar.bz2``. Now activate the addon which should
appear in the “User” categorie.. It needs a recent version of Blender in
order to work (2.80 or newer).

Note also that you need to have the Numpy Python module to be installed.

Information about Splash dependencies
-------------------------------------

Splash relies on a few libraries to get the job done. The mandatory
libraries are:

-  External dependencies:

   -  `OpenGL <http://opengl.org>`__, which should be installed by the
      graphic driver,
   -  `GSL <http://gnu.org/software/gsl>`__ (GNU Scientific Library) to
      compute calibration,

-  External dependencies bundled as submodules:

   -  `FFmpeg <http://ffmpeg.org/>`__ to read video files,
   -  `GLFW <http://glfw.org>`__ to handle the GL context creation,
   -  `GLM <http://glm.g-truc.net>`__ to ease matrix manipulation,
   -  `Snappy <https://code.google.com/p/snappy/>`__ to handle Hap codec
      decompression,
   -  `ZMQ <http://zeromq.org>`__ to communicate between the various
      process involved in a Splash session,
   -  `cppzmq <https://github.com/zeromq/cppzmq.git>`__ for its C++
      bindings of ZMQ

-  Dependencies built at compile-time from submodules:

   -  `doctest <https://github.com/onqtam/doctest/>`__ to do some unit
      testing,
   -  `ImGui <https://github.com/ocornut/imgui>`__ to draw the GUI,
   -  `JsonCpp <http://jsoncpp.sourceforge.net>`__ to load and save the
      configuration,
   -  `stb_image <https://github.com/nothings/stb>`__ to read images.

Some other libraries are optional:

-  External dependencies:

   -  `libshmdata <http://gitlab.com/sat-metalab/shmdata>`__ to read
      video flows from a shared memory,
   -  `portaudio <http://portaudio.com/>`__ to read and output audio,
   -  `Python <https://python.org>`__ for scripting capabilities,
   -  `GPhoto <http://gphoto.sourceforge.net/>`__ to use a camera for
      color calibration.

-  Dependencies built at compile-time from submodules:

   -  `libltc <http://x42.github.io/libltc/>`__ to read timecodes from
      an audio input,

Also, the `Roboto <https://www.fontsquirrel.com/fonts/roboto>`__ font is
used and distributed under the Apache license.

By default Splash is built and linked against the libraries included as
submodules, but it is possible to force it to use the libraries
installed on the system. This is described in the :ref:`Compile manually` section.

