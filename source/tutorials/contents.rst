Tutorials
=========
   
This section shows different ways of using Splash through examples. 

The examples are grouped in two sections: essential and intermediate usages. As
the names suggest, essential usages relate to easy use cases of Splash,
while intermediate usages relate to less common and more complex use cases.

All these examples rely on the Blender exporter, so you need to install
`Blender <https://blender.org>`__ as well as the :ref:`Blender addon`.

For instructions about downloading and installing Splash, please :doc:`read the installation page <../install/contents>`.


Essential usages
----------------

.. toctree::
   :maxdepth: 1
   :glob:

   first_steps   
   splash_configuration
   single_projector_example
   multi_projector_example
   
Intermediate usages
-------------------

.. toctree::
   :maxdepth: 1
   :glob:
   
   shape_placement
   virtual_probe
   calimiro_calibration

