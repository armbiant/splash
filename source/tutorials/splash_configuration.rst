Introduction to configuring Splash
==================================

This tutorial explains the basics to configuring Splash from the ground up, entirely from the Splash user interface. It covers the following:

-  configuring the video inputs and outputs
-  loading the assets (3D model, image, video, …)
-  saving/loading the configuration

To do this tutorial, you will need:

-  Splash installed on your computer
-  knowledge of how your display devices (screens, projectors) are configured

Note that the calibration process is not explained in this tutorial, please refer to the :doc:`single projector <first_steps>` and the :doc:`multi projectors <multi_projector_example>` examples for details in this regard.

Since the goal of this tutorial is to get to know Splash, there is no need for a videoprojector when following this tutorial.

Create a new configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^

In this section you will create a new configuration to work with. To create it, we will start from the default configuration and build on it:

-  run Splash
-  select the window containing the user interface
-  open the save configuration dialog using the `File > Save configuration as...` menu
-  navigate to where you want to save the file
-  enter a filename in the `Filename` field
-  press the `Enter` key to save the file.

.. figure:: ../_static/images/splash_save_config_as.jpg
   :alt: Save configuration as menu and dialog box

   Save configuration as menu and dialog box

Now reload the configuration:

-  show the open configuration dialog using the `File > Open configuration` menu
-  navigate to where the configuration file was previously saved
-  double click on the file to open it

You will notice that everything is black. That is because the asset files (for the mesh and the image) have not been copied along while saving the configuration to a new file. This will be taken care of in the following sections.

In the meantime, you can show the default mesh geometry by activating the wireframe view, by clicking the `Wireframe / Textured` button in the user interface.

.. figure:: ../_static/images/splash_set_wireframe.jpg
   :alt: Set rendering to wireframe

   Set rendering to wireframe


Navigate in the Graph tabulation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We will learn how to navigate and control Splash's scene graph through the UI `Graph` tabulation:

-  show the user interface by pressing `Ctrl + Tab`
-  click on the Graph tabulation to display the scene graph on the left side. On the right side will be displayed the attributes for currently selected objects.
-  click on the `image` node in the graph. Its attributes will now be shown on the right.

Now that the scene graph is visible, try moving around with these possible actions:
-  navigate in the graph by dragging it while pressing the middle mouse button pressed, or by pressing `Alt` while pressing the right mouse button.
-  move the nodes by dragging them with the left mouse button pressed
-  you can also select an object by looking for it in the `Object list`, under the scene graph.

.. figure:: ../_static/images/splash_default_config_graph.jpg
   :alt: Graph tabulation for the default configuration

   Graph tabulation for the default configuration


Case study with two video projectors and one screen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To continue this tutorial we will consider a simple case: a setup with one screen to show the user interface, and two video-projectors. The displays all have the same resolution of `1920x1080`.

You need to configure your system such that all outputs are set next to each other. Since this step depends on the specific hardware used, we cannot cover for all cases. Here are two examples, one using the `arandr` tool and the other using `nvidia-settings`.

On the `arandr` configuration it would look like that:

.. figure:: ../_static/images/arandr_two_projectors.jpg
   :alt: Display configuration using arandr
   
   Display configuration using arandr

For NVIDIA hardware using the `nvidia-settings` tool, it would look like that:

.. figure:: ../_static/images/nvsettings_two_projectors.jpg
   :alt: Display configuration using nvidia-settings
   
   Display configuration using nvidia-settings

Notice how all the displays are placed one next to another. In each case the first display is the screen, and the other two are the video projectors. Make sure that there is no gap between the displays.


Adapt Splash configuration to the hardware setup
------------------------------------------------

Here we will modify the previously-created Splash configuration to match the case-study.

We need a new camera to show the content onto the second videoprojector:

-  first, create a camera by cliking on the `camera` option, located in the `Add an object` list above the graph. A new node named `camera_1` will appear in the graph
-  select the new node by `left-clicking` on it
-  create a link between `camera_1` and `window_1` by `Shift + left-clicking` on the `window_1` node

At this point, some nodes might overlap. To have a clearer view over the scene graph, you can press the `Reorder graph` button at the bottom of the scene graph.

Now let's show something on the camera:

-  select the `object` node by `left-clicking` on it
-  with `Shift` pressed, `left-click` on `camera_1` to create a link to it from the `object` node

.. figure:: ../_static/images/splash_graph_add_camera.jpg
   :alt: Drop down menu to add a window

   Drop down menu to add a window

For now the window is not correctly located, and the cameras do not have the correct resolution:

-  select the `window_1` node
-  change its `size` attribute to `3840` and `1080`. It holds two `1920x1080` image, hence the `3840`.
-  change its `position` attribute to `1920` and `0`, considering your displays were configured as previously described.
-  select the `cam1` node
-  change its `size` attribute to `1920` and `1080`
-  select the `camera_1` node
-  change its `size` attribute to `1920` and `1080`

The scene graph should now look like this:

.. figure:: ../_static/images/splash_two_projectors_graph.jpg
   :alt: Resulting scene graph

   Resulting scene graph

And the `window_1` window should look more or less like this:

.. figure:: ../_static/images/splash_two_outputs_result.jpg
   :alt: Resulting window for two projectors

   Resulting window for two projectors


Select the 3D model and the visual input
----------------------------------------

The configuration is mostly complete. Now what is needed is a 3D model of the projection surface, and a visual input to be shown. For that we will use the default assets accompagnying Splash.

-  download the default assets from `Splash repository <https://gitlab.com/sat-metalab/splash/-/tree/master/data/share/splash>`__: `color_map.png` and `wall_corner.obj`. Let say they were downloaded in the `~/Downloads` directory.
-  go to the `Medias` tabulation. The `image` node attributes should be shown.
-  using the path attribute and its `…` button, open the file dialog and go to `~/Downloads`. Double click on the `color_map.png` file to open it.
-  go to the `Meshes` tabulation. The `mesh` node attributes should be shown.
-  using the path attribute and its `…` button, open the file dialog and go to `~/Downloads`. Double click on the `wall_corner.obj` file to open it.

You should now be capable of adapting the configuration to your needs. Next, you should go to the :doc:`single projector example <first_steps>` tutorial.
