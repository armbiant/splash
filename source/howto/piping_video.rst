Piping video through v4l2loopback or shmdata
============================================

It may happen that the capture card does not comply with the
Video4Linux2 API. For example, Blackmagic distributes proprietary
drivers with their capture card. As such they are not compatible with
Splash out of the box, but it is still possible to use them. The trick
is to transmit the data through
`v4l2loopback <https://github.com/umlaeute/v4l2loopback>`__ or
`shmdata <https://gitlab.com/sat-metalab/shmdata>`__, and read the
capture card from `FFmpeg <https://ffmpeg.org>`__ or
`GStreamer <https://gstreamer.freedesktop.org/>`__.

Piping through v4l2loopback
---------------------------

The goal of v4l2loopback is to simulate a V4L2 device (simular to a
webcam or a capture card), which is feed from a software. First, you
have to install and load it onto your computer. On Ubuntu:

.. code:: bash

   sudo apt install v4l2loopback-utils
   sudo modprobe v4l2loopback

A new device should have appeared as ``/dev/videoX``, ``X`` being the id
of the device. Let’s say you have no other V4L2 device, the path would
then be ``/dev/video0``.

Then you can launch a GStreamer pipeline (provided that it is installed,
which is outside of the scope of this documentation), and feed the
device a test stream or the stream from a Blackmagic capture card:

.. code:: bash

   # Test stream
   gst-launch-1.0 -v videotestsrc ! v4l2sink device=/dev/video0

   # Capture from a Blackmagic card
   gst-launch-1.0 -v decklinkvideosrc ! v4l2sink device=/dev/video0

Then in Splash you can create a V4L2 media and grab the feed from the
``/dev/video0`` device

Piping through shmdata
----------------------

Shmdata is a library which gives abilities similar to Syphon on macOS, but
more general as it can carry any kind of data. First you have to make
sure that it is installed on your system and that Splash has been
compiled against it. Grab it `here <https://gitlab.com/sat-mtl/tools/shmdata>`__ and follow the 
`readme <https://gitlab.com/sat-mtl/tools/shmdata/tree/master/README.html>`__
to compile and install it. Then recompile and install Splash as
described on the :doc:`Installation <../install/contents>` page.

You can then launch a GStreamer pipeline and send any feed through
Shmdata:

.. code:: bash

   # Test stream
   gst-launch-1.0 -v videotestsrc ! shmdatasink socket-path=/tmp/gst-feed

   # Capture from a Blackmagic card
   gst-launch-1.0 -v decklinkvideosrc ! shmdatasink socket-path=/tmp/gst-feed

Then in Splash you can create a ‘video through shared memory’ object and
set ``/tmp/gst-feed`` as the filed path. The video feed should display
instantaneously.


