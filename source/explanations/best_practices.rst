Best practices
==============

Preferred codecs
----------------

Splash has been built to handled very large video feeds, but not all
video codecs can manage high resolution files correctly. Internally,
Splash uses `FFmpeg <http://ffmpeg.org/>`__ to read the video files. The
suggested codecs, based on the resolution, are the following:

================ ===== ==== ======== ===
Max. resolution  MJPEG HapQ HapAlpha Hap
================ ===== ==== ======== ===
3072x3072 @ 30Hz OK    OK   OK       OK
4096x4096 @ 30Hz       OK   OK       OK
Anything higher             OK       OK
================ ===== ==== ======== ===

Note that all these codecs have no inter-frame compression. This does
not mean that codecs with inter-frame compression can not be read, but
the listed codecs have the best performances for very high resolutions
videos. Also, codecs with inter-frame compression are not tested and
some issues can arise with these.

To convert your existing video files to one of these codecs, it is
suggested to use `FFmpeg <https://ffmpeg.org>`__. In the following
commands, replace ${INPUT_FILE} and ${OUTPUT_FILE} with the source video
file name and the output video file name.

.. code:: bash

   ffmpeg -i ${INPUT_FILE} -c:v mjpeg -c:a copy ${OUTPUT_FILE}
   ffmpeg -i ${INPUT_FILE} -c:v hap -format hap_q -chunks 8 -c:a copy ${OUTPUT_FILE}
   ffmpeg -i ${INPUT_FILE} -c:v hap -format hap_alpha -chunks 8 -c:a copy ${OUTPUT_FILE}
   ffmpeg -i ${INPUT_FILE} -c:v hap -format hap -chunks 8 -c:a copy ${OUTPUT_FILE}

--------------

General
-------

-  Splash is designed with real-world scale in mind: it is strongly
   advised to use the meter as the unit for the 3D models, to prevent
   issues with near or far clipping.
-  The basis used in Splash is so that the XY plane is horizontal, and
   the Z axis is vertical. The 3D models should be exported with this
   basis to prevent navigation issues while calibrating.
-  For performance reasons, it is strongly advised to use no more than
   one window for each graphic card. Each additional window implies some
   context switching, which degrades performances.

--------------

Default values file
-------------------

It is possible to override the hardcoded default values for the various
objects by setting up a default values files. Splash looks for such a
file based on the ``${SPLASH_DEFAULTS}`` environment variable, which
should point to a Json file with the correct structure. It is ones
responsibility to set this environment variable, for example by
specifying it right before running Splash:

.. code:: bash

   export SPLASH_DEFAULTS="${HOME}/.config/splash"

In this example, the configuration file should be located in
``${HOME}/.config/splash``, and look like this:

.. code:: json

   {
     "image_ffmpeg" : {
       "bufferSize" : 4096
     }
   }

Here, the ``bufferSize`` parameter of the ``image_ffmpeg`` type (which
is responsible for reading videos) is set to 4096MB by default,
overriding the hardcoded value of 512MB. Note that the object type may
differ from the type shown in the GUI, please refer to the
:doc:`documentation <technical_information>` to
find the correct type.

--------------

Video overlay
-------------

Objects can have multiple images (or videos) linked to them. As of yet
there is no blending options, but if the second image has an alpha
channel it will be used as a mask to merge the two sources. The alpha
channel of the first image is completely discarded.

For still images, it is advised to use the PNG file format. For videos
the Hap Alpha codec is the best choice, converted from a sequence of PNG
files with transparency. See the `FAQ <#preferred-codecs>`__ for
information about how to convert from a PNG sequence to a Hap video
file.

To connect an additional image to an object, on the GUI control panel:

-  create a new image / image_ffmpeg / image_shmdata
-  select the file to play by setting its path in the image attributes
-  shift+click on the target object to link the image to it
