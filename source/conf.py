# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Splash'
copyright = '2021, Metalab'
author = 'Metalab'

# The full version, including alpha/beta/rc tags
release = '0.1'

# -- General configuration ---------------------------------------------------

# landing page

master_doc = 'contents'

templates_path = ['_templates/']

# def setup(app):
# app.add_css_file('css/landingpage.css')


# -- LOCALES ---------------------------------------------------------------

locale_dirs = ['locale/']   # path is example but recommended.
gettext_compact = False     # optional.



# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
# https://www.sphinx-doc.org/en/master/usage/markdown.html

extensions = [
    'recommonmark',
    'sphinx.ext.autosectionlabel'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['docs_todo/*']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "furo"


# -- add logo to the docs, not only the landing page
# how to : https://pradyunsg.me/furo/customisation/logo/

html_theme_options = {
    "light_logo": "images/splash_logo.png",
    "dark_logo": "images/splash_logo.png",
}


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# sidebar

html_sidebars = {
  "*" :["sidebar/brand.html",
   "sidebar/search.html",
  "sidebar/scroll-start.html",
   "sidebar/navigation.html",
  "sidebar/scroll-end.html"]}


html_favicon = '_static/favicon.ico'

# -- LOCALE MENU -------------------------------------------------------------

# POPULATE LINKS TO OTHER LANGUAGES

try:
   html_context
except NameError:
   html_context = dict()

# SET CURRENT_LANGUAGE
if 'current_language' in os.environ:
   # get the current_language env var set by buildDocs.sh
   current_language = os.environ['current_language']
else:
   # the user is probably doing `make html`
   # set this build's current language to english
   current_language = 'en'
 
# tell the theme which language to we're currently building
html_context['current_language'] = current_language

html_context['languages'] = []
html_context['languages'].append( ('en', 'en/') )
 
languages = [lang.name for lang in os.scandir('locale') if lang.is_dir()]
for lang in languages:
   html_context['languages'].append( (lang, lang + '/') )

