# Splash - website - documentation

Version [develop](https://gitlab.com/sat-mtl/tools/splash/splash/-/tree/develop).

This is the repository for building the [Splash docs website](https://sat-mtl.gitlab.io/documentation/splash).

The [Splash code repo is here on Gitlab](https://www.gitlab.io/sat-mtl/tools/splash/splash).

The documentation website is based on [the template-website project](https://gitlab.com/sat-mtl/documentation/template-website).

## Contributing

We are currently working on identifying `good first issues` for improving the Splash documentation. Look in [issues for this tag](https://gitlab.com/sat-mtl/documentation/splash/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=good%20first%20issue&first_page_size=20).

We welcome [bug reports](https://gitlab.com/sat-mtl/documentation/splash/-/issues) and we invite you to [start a conversation with us on Discourse](https://discourse.sat.qc.ca) if you wish to help us improve Splash documentation.

## License

The documentation is licensed under a [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

All the source code (including examples and excerpt from the software) is licensed under the [GNU Public License version 3 (GPLv3)](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Code of Conduct

By participating in this project, you agree to abide by the [Code of Conduct](CODE_OF_CONDUCT.md). We expect all contributors to follow the [Code of Conduct](CODE_OF_CONDUCT.md) and to treat fellow humans with respect. It must be followed in all your interactions with the project.

